package com.ridho.udacity.myapplication

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setListeners()
    }

    private fun setListeners() {
        val clickAbleViews: List<View> = listOf(
            box_one_text_view,
            box_two_text_view,
            box_three_text_view,
            box_four_text_view,
            box_five_text_view,
            button_cyan,
            button_magenta,
            button_green
        )

        for (item in clickAbleViews) {
            item.setOnClickListener { makeColored(it) }
        }
    }

    private fun makeColored(view: View) {
        when (view.id) {
            R.id.box_one_text_view -> {
                view.setBackgroundColor(Color.DKGRAY)
                Toast.makeText(applicationContext, "yuhu", Toast.LENGTH_SHORT).show()
            }

            R.id.box_two_text_view -> view.setBackgroundColor(Color.YELLOW)
            R.id.box_three_text_view -> view.setBackgroundColor(Color.RED)
            R.id.box_four_text_view -> view.setBackgroundColor(Color.BLACK)
            R.id.box_five_text_view -> view.setBackgroundColor(Color.BLUE)
            R.id.button_cyan -> box_one_text_view.setBackgroundColor(Color.CYAN)
            R.id.button_magenta -> box_two_text_view.setBackgroundColor(Color.MAGENTA)
            R.id.button_green -> box_three_text_view.setBackgroundColor(Color.GREEN)

            else -> view.setBackgroundColor(Color.LTGRAY)
        }
    }
}